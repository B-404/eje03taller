<?php




if (!isset($_GET['controller'])) {
    // si tiene vavia la variable controller, enviaremos a la vista de inicio.
    require_once "controller/inicio.controlador.php";
    // creamos una instancia de la clase inicio.controlador
    $controlador = new InicioControlador();
    call_user_func(array($controlador,"inicio"));
}else{
    // el usuario selecciona una opcion
    $controlador = $_GET['controller'];
    require_once "controller/$controlador.controlador.php";
    $controlador = $controlador."Controlador";
    $controlador = new $controlador;

    // vamos anidar un if else para preguntar

    if (isset($_GET['metodo'])) {
        $metodo = $_GET['metodo'];
    }else{
        $metodo = "inicio";
    }

    call_user_func(array($controlador,$metodo));
}