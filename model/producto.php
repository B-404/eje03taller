<?php

//clase pojo es una clase plana que incluye get and set.

require_once 'bd.php';

Class Producto{

 private $codigo;
 private $nombre;
 private $precio;
 private $stock;


 private $pdo;

 public function __construct(){
   $this->pdo = Bdconexion::Conectar();
 }



 public function getCodigo() :?int{
   return $this->codigo;
 }

 public function setCodigo(int $codigo){
   $this->codigo = $codigo;
 }

 public function getNombre() :?string{
   return $this->nombre;
 }

 public function setNombre(string $nombre){
    $this->nombre = $nombre;
 }

 public function getPrecio() :?int{
   return $this->precio;
 }

 public function setPrecio(int $precio){
   $this->precio = $precio;
 }

 public function getStock() :?int{
   return $this->stock;
 }

 public function setStock(int $stock){
   $this->stock = $stock;
 }

 public function Insertar(Producto $producto){
    try {
      $consulta = "insert into tblproducto (codigo,nombre,precio,stock) values (?,?,?,?)";
      $this->pdo->prepare($consulta)->execute(array($producto->getCodigo(),$producto->getNombre(),$producto->getPrecio(),$producto->getStock()));
    } catch (Exception $e) {
      die($e->getMessage());
    }
 }

}